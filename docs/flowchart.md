# Maschinencontrol

– Maschinenzugangsberechtigungssystem



```flow
start=>start: NFC presented

checkId=>operation: check if ID is valid
checkPermission=>operation: check if user has Permission
retrieveUserdata=>operation: retrieve user data
displayUserdata=>operation: display user data
updateOperatingTime=>operation: update operating hours
switchRelayOn=>operation: switch relay on
switchRelayOff=>operation: switch relay off

chIdValid=>condition: ID valid?
chHasPermission=>condition: user has
permission
chLoggedOut=>condition: logout pressed?

reportError=>subroutine: Report error

end=>end

start->checkId
checkId->chIdValid
chIdValid(yes)->checkPermission
chIdValid(no)->reportError

checkPermission->chHasPermission
chHasPermission(yes)->retrieveUserdata
chHasPermission(no)->reportError

retrieveUserdata->displayUserdata
displayUserdata->switchRelayOn
switchRelayOn->chLoggedOut
chLoggedOut(no)->updateOperatingTime
updateOperatingTime(top)->displayUserdata
chLoggedOut(yes)->switchRelayOff
switchRelayOff->end
```



