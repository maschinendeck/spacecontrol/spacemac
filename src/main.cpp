#include <Arduino.h>
#include <memory>

#include "SpaceMAC.hpp"

std::unique_ptr<spacemac::SpaceMAC> app;

void setup() {
	app = std::make_unique<spacemac::SpaceMAC>();
}

void loop() {
	app->update();
}