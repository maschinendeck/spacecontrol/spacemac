#pragma once

#include "Module.hpp"

namespace spacemac {
	class SpaceMAC;

	namespace modules {

		class NFC : public Module {
			public:
				NFC(spacemac::SpaceMAC& app) : Module(app) {

				}

				virtual void update(float deltaTime) {}
		};

	}

}