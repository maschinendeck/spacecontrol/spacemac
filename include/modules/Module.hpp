#pragma once

namespace spacemac {
	class SpaceMAC;
}

namespace spacemac::modules {

	class Module {
		protected:
			SpaceMAC& app;
		public:
		 	Module(SpaceMAC& app) : app(app) {}
			Module(const Module&) = delete;
			Module(Module&&) = default;
			virtual ~Module() {};

			virtual void update(float deltaTime) = 0;
	};

};