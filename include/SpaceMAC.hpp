#pragma once

#include <memory>

#include "Arduino.h"
#include "SPI.h"
#include "modules/NFC.hpp"

namespace spacemac {

	class SpaceMAC {
		private:
			std::unique_ptr<modules::NFC> nfc_;

		public:
			SpaceMAC();

			void update();
	};

}